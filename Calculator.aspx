﻿<%@ Page Title="Calculator" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Calculator.aspx.cs" Inherits="WebApplication1.Calculator" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <form id="form1">
        <div>
            <asp:Label ID="Label2" runat="server" CssClass="form-control" Text="Kalkulator" ForeColor="#003300" Width="120px"></asp:Label>
            <br />
        </div>
        <p>
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        </p>
        <table>
            <tr>
                <td><button class="btn btn-success numbers">7</button> </td>
                <td><button class="btn btn-success numbers">8</button></td>
                <td><button class="btn btn-success numbers">9</button></td>
                <td><button class="btn btn-success numbers">/</button></td>
                
            </tr>
            <tr>
                <td><button class="btn btn-success numbers">4</button></td>
                <td><button class="btn btn-success numbers">5</button></td>
                <td><button class="btn btn-success numbers">6</button></td>
                <td><button class="btn btn-success numbers">x</button></td>
                
            </tr>
            <tr>
                <td><button class="btn btn-success numbers">1</button></td>
                <td><button class="btn btn-success numbers">2</button></td>
                <td><button class="btn btn-success numbers">3</button></td>
                <td><button class="btn btn-success numbers">-</button></td>
                
            </tr>
            <tr>
                
                <td><button class="btn btn-success numbers">0</button></td>
                <td><button class="btn btn-success numbers">(</button></td>
                <td><button class="btn btn-success numbers">)</button></td>
                <td><button class="btn btn-success numbers">+</button></td>
            </tr>
            <tr>
                <td><asp:Button Text="C" runat="server" CssClass="btn btn-danger" OnClick="Unnamed5_Click" /></td>
                <td><asp:Button Text="CE" runat="server" CssClass="btn btn-danger" OnClick="Unnamed6_Click" /></td>
                <td colspan="2"><asp:Button Text="=" runat="server" CssClass="btn btn-danger" OnClick="getResult" Width="68px" /></td>
            </tr>
        </table>
    </form>

    <script>
        $('.numbers').click(function (e) {
            e.preventDefault();
            console.log($(this).text());
            $('#MainContent_TextBox1').val($('#MainContent_TextBox1').val() + $(this).text());
        });


    </script>

 </asp:Content>