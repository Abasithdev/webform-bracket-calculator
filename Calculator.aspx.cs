﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication1.Models;

namespace WebApplication1
{
    public partial class Calculator : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Label2.Text = TextBox1.Text + " Welcome to vs";
        }

        protected void Unnamed5_Click(object sender, EventArgs e)
        {
            string text = TextBox1.Text;
            if (text.Length > 0)
            {
                TextBox1.Text = text.Substring(0, text.Length - 1);
            }
            
        }

        protected void Unnamed6_Click(object sender, EventArgs e)
        {
            TextBox1.Text = "";
        }

        protected void getResult(object sender, EventArgs e)
        {
            Calculatorv2 calc2 = new Calculatorv2();
            try
            {
                TextBox1.Text = calc2.MainCalculate(TextBox1.Text.ToCharArray()).ToString();
            }
            catch(Exception exp)
            {
                TextBox1.Text = "Di cek kembali inputan anda";
            }
            
        }
    }
}